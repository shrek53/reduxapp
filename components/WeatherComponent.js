import React, { Component } from 'react';
import { ActivityIndicator, ListView, Text, View , StyleSheet, TouchableHighlight } from 'react-native';
// import  Row from './rowconst';
export class WeatherInfoGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        }
    }

    componentDidMount() {
        return fetch('http://api.openweathermap.org/data/2.5/find?lat=23.68&lon=90.35&cnt=50&appid=e384f9ac095b2109c751d95296f8ea76')
            .then((response) => response.json())
            .then((responseJson) => {
                let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                this.setState({
                    isLoading: false,
                    dataSource: ds.cloneWithRows(responseJson.list),
                }, function() {
                    // do something with new state
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <View style={{flex: 1, paddingTop: 20}}>
                <ListView
                    style={weather_group_styles.weather_group_item}
                     dataSource={this.state.dataSource}
                    renderRow={(rowData) =>  <Row {...rowData} /> }
                    renderSeparator={(sectionId, rowId) => <View key={rowId} style={weather_group_styles.weather_group_item_separator} />}ß
                />
            </View>
        );
    }
}




const Row = (props) => (
    <View style={styles.container} >
        <TouchableHighlight
            onPress={ () => {
                this.props.navigation.navigate('Profile');
            }
            }
        >
        <Text style={styles.text}>
            {`${props.name} \n ${props.weather[0].main}`}
        </Text>
        </TouchableHighlight>
        <Text style={styles.temperature_style}>
            {`${ props.main.temp}`}
        </Text>
    </View>
);


const weather_group_styles = StyleSheet.create({
    weather_group_item: {
        flex: 1,
        marginTop: 20,
    },
    weather_group_item_separator: {
            flex: 1,
            height: StyleSheet.hairlineWidth,
            backgroundColor: '#8E8E8E',

    },
});



const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 5,
        flexDirection: 'row',
        // alignItems: 'center',
    },
    text: {
        // marginLeft: 5,
        fontSize: 16,
        // fontFamily : 'Roboto',
        alignItems: 'flex-start',
    },
    photo: {
        height: 40,
        width: 40,
        borderRadius: 20,
    },
    temperature_style:{
        flexDirection: 'row',
        alignItems: 'flex-end',
        fontSize: 24,
        // justifyContent: 'flex-end',
    }
});