import React from 'react';
import { View, Button, TouchableHighlight, Text, StyleSheet, Image } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 5,
        flexDirection: 'row',
        // alignItems: 'center',
    },
    text: {
        // marginLeft: 5,
        fontSize: 16,
        // fontFamily : 'Roboto',
        alignItems: 'flex-start',
    },
    photo: {
        height: 40,
        width: 40,
        borderRadius: 20,
    },
    temperature_style:{
        flexDirection: 'row',
        alignItems: 'flex-end',
        fontSize: 24,
        // justifyContent: 'flex-end',
}
});

const Row = (props) => (
    <View style={styles.container} >
        {/*<Image source={{ uri: props.picture.large}} style={styles.photo} />*/}
        <Text style={styles.text}>
            {`${props.name} \n ${props.weather[0].main}`}
        </Text>
        <Text style={styles.temperature_style}>
            {`${ props.main.temp}`}
        </Text>
     </View>
);

export default Row;