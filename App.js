import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import {StyleSheet, Text, View, Button, Alert} from 'react-native';

import {WeatherInfoGroup} from './components/WeatherComponent';


import {
    StackNavigator,
} from 'react-navigation';


class MainScreen extends React.Component {
    static navigationOptions = {
        title: 'Welcome',

    };
    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Button
                    title="Go to mapview"
                    onPress={() =>
                        navigate('Profile', { name: 'Jane' })
                    }
                />
                <WeatherInfoGroup />
            </View>
        );
    }
}

class ProfileScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.name,
    });

    render() {
        const {goBack} = this.props.navigation;
        return (
            <Button
                title="Go back"
                onPress={() => goBack()}
            />
        );
    }
}


const BasicApp = StackNavigator({
    Main: {screen: MainScreen},
    Profile: {screen: ProfileScreen},
});


export default class App extends Component {
    render() {
        return (

            <BasicApp/>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

//
// <View style={styles.container}>
//   <WeatherInfoGroup />
// </View>

